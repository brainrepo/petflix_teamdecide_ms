const fs = require("fs");

const fastify = require("fastify")({
    logger: true
})

fastify.register(require("fastify-cors"), {
    origin: "*"
})


fastify.get("/:film", (req, res) => {
    const ID = req.params.film;
    fs.readFile("./database.json", (err, data) => {
        res.type("application/json").code(200);
        const films = JSON.parse(data).filter( e => e.imdbID === ID);
        res.send(films[0]);
    })
})


fastify.get("/", (req, res) => {
    fs.readFile("./database.json", (err, data) => {
        res.type("application/json").code(200);
        const films = JSON.parse(data).sort(_ => 0.5 * Math.random());
        res.send(films.slice(0,30));
    })
})

fastify.listen(3000, '0.0.0.0', (err, address) => {
    if (err) throw err;
    fastify.log.info(`decide api on ${address}`);
})